module.exports.intent = function() {
    var speech = ""

    if (this.attributes['list']) {
        if (this.attributes['list'].indexOf(this.event.request.intent.slots.ListItem.value) > -1) {
            speech = `${this.event.request.intent.slots.ListItem.value} is already on your list.`
        } else {
            this.attributes['list'].push(this.event.request.intent.slots.ListItem.value)
            speech = `I added ${this.event.request.intent.slots.ListItem.value} to your list.`
        }
    } else {
        this.attributes['list'] = [this.event.request.intent.slots.ListItem.value]
        speech = `I created a new list and added ${this.event.request.intent.slots.ListItem.value} to it.`
    }

    this.response.speak(speech)
    this.emit(":responseReady")
}
