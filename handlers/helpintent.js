module.exports.intent = function() {
    var speech = ""

    speech = `Welcome to the help menu. MaiList has three streamlined features. Adding to your list, deleting from your list, and displaying your list.
              If at any point you want me to stop, just say <break time='100ms'/>Alexa, stop<break time='100ms'/>.
              You can add to your list by saying <break time='100ms'/>Alexa, ask mylist to add something<break time='100ms'/>.
              You can delete from your list by saying <break time='100ms'/>Alexa, ask mylist to delete the last item on my list<break time='100ms'/>.
              And you can hear what's on your list by saying <break time='100ms'/>Alexa, ask mylist what's on my list<break time='100ms'/>.`

    this.response.speak(speech)
    this.emit(':responseReady')
}
