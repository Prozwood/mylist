module.exports.intent = function() {
    var speech = ""

    if (this.attributes['list']) {
        if (this.attributes['list'].length > 0) {
            var listItem = this.event.request.intent.slots.ListItem.value

            if (listItem === "last") {
                this.attributes['list'].splice(this.attributes['list'].length - 1, 1)
                speech = `Deleted the last item from your list.`
            } else if (listItem === "second" || listItem === "2nd") {
                if (2 <= this.attributes['list'].length) {
                    this.attributes['list'].splice(1, 1)
                    speech = `Deleted the second item from your list.`
                } else {
                    if (this.attributes['list'].length === 1) {
                        speech = `Your list only has ${this.attributes['list'].length} item.`
                    } else {
                        speech = `Your list only has ${this.attributes['list'].length} items.`
                    }
                }
            } else if (listItem === "sixt" || listItem === "sixth" || listItem === "6th") {
                if (6 <= this.attributes['list'].length) {
                    this.attributes['list'].splice(5, 1)
                    speech = `Deleted the sixth item from your list.`
                } else {
                    if (this.attributes['list'].length === 1) {
                        speech = `Your list only has ${this.attributes['list'].length} item.`
                    } else {
                        speech = `Your list only has ${this.attributes['list'].length} items.`
                    }
                }
            } else {
                stIndex = listItem.indexOf("st")
                ndIndex = listItem.indexOf("nd")
                rdIndex = listItem.indexOf("rd")
                thIndex = listItem.indexOf("th")
                indecies = [stIndex, ndIndex, rdIndex, thIndex]

                for (var i = 0; i < indecies.length; i++) {
                    if (indecies[i] > -1 && !isNaN(parseInt(listItem.substring(0, indecies[i])))) {
                        if (parseInt(listItem.substring(0, indecies[i])) - 1 < this.attributes['list'].length) {
                            this.attributes['list'].splice(parseInt(listItem.substring(0, indecies[i])) - 1, 1)
                            speech = `Deleted ${listItem} from your list.`
                        } else {
                            if (this.attributes['list'].length === 1) {
                                speech = `Your list only has ${this.attributes['list'].length} item.`
                            } else {
                                speech = `Your list only has ${this.attributes['list'].length} items.`
                            }
                        }
                    }
                }

                if (speech === "") {
                    var listItemIndex = this.attributes['list'].indexOf(listItem)

                    if (listItemIndex > -1) {
                        this.attributes['list'].splice(listItemIndex, 1)
                        speech = `Deleted ${listItem} from your list.`
                    } else {
                        speech = `Sorry, either that item was not on your list, or I didn't understand you. Please try again.`
                    }
                }
            }
        } else {
            speech = `You haven't added anything to your list yet.`
        }
    } else {
        this.attributes['list'] = []
        speech = `You haven't added anything to your list yet.`
    }

    this.response.speak(speech)
    this.emit(":responseReady")
}
