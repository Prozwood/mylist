module.exports.intent = function() {
    var speech = ""

    if (this.attributes['list']) {
        if (this.event.request.intent.slots.Spot) {
            if (this.event.request.intent.slots.Spot.value) {
                spot = this.event.request.intent.slots.Spot.value

                if (spot === "last") {
                    speech = `${this.attributes['list'][this.attributes['list'].length - 1]}<break time='100ms'/> is last on your list.`
                } else if (spot === "second" || spot === "2nd") {
                    if (2 <= this.attributes['list'].length) {
                        this.attributes['list'].splice(1, 1)
                        speech = `Deleted the second item from your list.`
                    } else {
                        if (this.attributes['list'].length === 1) {
                            speech = `Your list only has ${this.attributes['list'].length} item.`
                        } else {
                            speech = `Your list only has ${this.attributes['list'].length} items.`
                        }
                    }
                } else if (spot === "sixt" || spot === "sixth" || spot === "6th") {
                    if (6 <= this.attributes['list'].length) {
                        speech = `${this.attributes['list'][5]}<break time='100ms'/> is sixth on your list.`
                    } else {
                        if (this.attributes['list'].length === 1) {
                            speech = `Your list only has ${this.attributes['list'].length} item.`
                        } else {
                            speech = `Your list only has ${this.attributes['list'].length} items.`
                        }
                    }
                } else if (isNaN(parseInt(spot))) {
                    stIndex = spot.indexOf("st")
                    ndIndex = spot.indexOf("nd")
                    rdIndex = spot.indexOf("rd")
                    thIndex = spot.indexOf("th")
                    indecies = [stIndex, ndIndex, rdIndex, thIndex]

                    for (var i = 0; i < indecies.length; i++) {
                        if (indecies[i] > -1) {
                            if (parseInt(spot.substring(0, indecies[i])) - 1 < this.attributes['list'].length) {
                                speech = `${this.attributes['list'][parseInt(spot.substring(0, indecies[i])) - 1]}<break time='100ms'/> is ${spot} on your list.`
                            } else {
                                if (this.attributes['list'].length === 1) {
                                    speech = `Your list only has ${this.attributes['list'].length} item.`
                                } else {
                                    speech = `Your list only has ${this.attributes['list'].length} items.`
                                }
                            }
                        }
                    }

                    if (speech === "") {
                        speech = "Sorry, I don't understand. Please try again."
                    }
                } else {
                    if (parseInt(spot) - 1 < this.attributes['list'].length) {
                        speech = `${this.attributes['list'][parseInt(spot) - 1]}<break time='100ms'/> is ${parseInt(spot)} on your list.`
                    } else {
                        if (this.attributes['list'].length === 1) {
                            speech = `Your list only has ${this.attributes['list'].length} item.`
                        } else {
                            speech = `Your list only has ${this.attributes['list'].length} items.`
                        }
                    }
                }
            } else {
                if (this.attributes['list'].length > 0) {
                    if (this.attributes['list'].length === 1) {
                        speech += `Your list only has <break time='100ms'/> ${this.attributes['list'][0]}.`
                    } else {
                        speech += "Your list has "

                        if (this.attributes['list'].length > 5) {
                            for (var i = 0; i < this.attributes['list'].length; i++) {
                                if (i === 4) {
                                    speech += `<break time='100ms'/> ${this.attributes['list'][i]}, and more.`
                                    break
                                } else {
                                    speech += `<break time='100ms'/> ${this.attributes['list'][i]},`
                                }
                            }
                        } else {
                            for (var i = 0; i < this.attributes['list'].length; i++) {
                                if (i === this.attributes['list'].length - 1) {
                                    speech += `and ${this.attributes['list'][i]}.`
                                } else {
                                    speech += `<break time='100ms'/> ${this.attributes['list'][i]}, `
                                }
                            }
                        }
                    }
                } else {
                    speech = "Your list is currently empty."
                }
            }
        } else {
            if (this.attributes['list'].length > 0) {
                if (this.attributes['list'].length === 1) {
                    speech += `Your list only has <break time='100ms'/> ${this.attributes['list'][0]}.`
                } else {
                    speech += "Your list has "

                    for (var i = 0; i < this.attributes['list'].length; i++) {
                        if (i === this.attributes['list'].length - 1) {
                            speech += `and ${this.attributes['list'][i]}.`
                        } else {
                            speech += `<break time='100ms'/> ${this.attributes['list'][i]}, `
                        }
                    }
                }
            } else {
                speech = "Your list is currently empty."
            }
        }
    } else {
        speech = `You haven't added anything to your list yet.`
    }

    this.response.speak(speech)
    this.emit(":responseReady")
}
