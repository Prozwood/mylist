module.exports.request = function() {
    if (!this.attributes['list']) {
        this.attributes['list'] = []
    }

    var speech = "Welcome to MaiList! What would you like to do?"
    var question = "Please say what you would like to do."

    this.response.speak(speech).listen(question)
    this.emit(':responseReady')
}
