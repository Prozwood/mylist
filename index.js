//EXTERNAL LIBRARY
const Alexa = require('alexa-sdk')

//DEFINE HANDLERS
var path = "handlers/"

var launch = require(path + 'launchrequest')
var add = require(path + 'addintent')
var del = require(path + 'deleteintent')
var list = require(path + 'listintent')
var end = require(path + 'endintent')
var help = require(path + 'helpintent')
var unhandled = require(path + 'unhandledintent')

//SET UP ALEXA-SDK
exports.handler = function(event, context, callback) {
    const alexa = Alexa.handler(event, context, callback)
    alexa.APP_ID = "amzn1.ask.skill.17a50520-caa6-4f36-91c0-5cb51ee07a18"
    alexa.dynamoDBTableName = "MyList"
    alexa.registerHandlers(handlers)
    alexa.execute()
}

//ASSIGN HANDLERS
const handlers = {
    //CUSTOM INTENTS
    'LaunchRequest': launch.request,
    'AddIntent': add.intent,
    'DeleteIntent': del.intent,
    'ListIntent': list.intent,

    //REQUIRED INTENTS
    'AMAZON.CancelIntent': end.intent,
    'AMAZON.HelpIntent': help.intent,
    'AMAZON.StopIntent': end.intent,

    //UNHANDLED EVENT
    'Unhandled': unhandled.intent
}
